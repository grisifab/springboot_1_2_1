package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot121Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot121Application.class, args);
	}

}
