package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.RequestBody;
import com.example.demo.dao.InstructorRepository;
import com.example.demo.models.Instructor;
import com.example.demo.models.InstructorDetail;

@RunWith(SpringRunner.class)
@DataJpaTest

class InstructorController {

	InstructorDetail idet1 = new InstructorDetail("mobby", "sbooby");
	InstructorDetail idet2 = new InstructorDetail("mobby2", "sbooby2");
	Instructor p1 = new Instructor("Morice", "Trintignant", "M.T.com", idet1);
	Instructor p2 = new Instructor("Boubou", "Foufou", "B.F.com", idet2);
	
	@Autowired
	private InstructorRepository instructorRepository;
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Test
	void testGetInstructors() {	
		entityManager.persist(p1);
		entityManager.persist(p2);
		
		List<Instructor> lp1 = instructorRepository.findAll();
		List<Instructor> lp2 = new ArrayList<Instructor>();
		
		for (Instructor instructor : lp1) {
			lp2.add(instructor);
		}
		assertThat(lp2.size()).isEqualTo(2);	
	}

	@Test
	void testGetInstructorById() {
		Instructor pSavInDb = entityManager.persist(p1);
		Instructor pFromDb = instructorRepository.getOne(pSavInDb.getId());
		assertEquals(pSavInDb,pFromDb);
		assertThat(pFromDb.equals(pSavInDb));
	}

	@Test
	void testSave() {
		Instructor pSavInDb = entityManager.persist(p1);
		Instructor pFromDb = instructorRepository.getOne(pSavInDb.getId());
		assertEquals(pSavInDb,pFromDb);
		assertThat(pFromDb.equals(pSavInDb));
	}

	@Test
	void testUpdateInstructor() {
		Instructor pSavInDb = entityManager.persist(p1);
		Instructor pFromDb = instructorRepository.getOne(pSavInDb.getId());
		pFromDb.setFirstName("Tiodio");
		entityManager.persist(pFromDb);
		pFromDb = instructorRepository.getOne(pSavInDb.getId());
		assertEquals(pFromDb.getFirstName(),"Tiodio");
	}

	@Test
	void testDeleteInstructor() {
		Instructor pSavInDb = entityManager.persist(p1);
		entityManager.persist(p2);
		entityManager.remove(pSavInDb);
		List<Instructor> lp1 = instructorRepository.findAll();
		List<Instructor> lp2 = new ArrayList<Instructor>();
		
		for (Instructor instructor : lp1) {
			lp2.add(instructor);
		}
		assertThat(lp2.size()).isEqualTo(1);
	}

}
